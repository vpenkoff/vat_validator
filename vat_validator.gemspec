Gem::Specification.new do |s|
  s.name        = 'vat_validator'
  s.version     = '1.0.5'
  s.date        = '2017-10-24'
  s.summary     = 'Validate VAT IDs using the European Commission service'
  s.description = 'Validate VAT IDs'
  s.authors     = ['Viktor Penkov']
  s.email       = 'vpenkoff@gmail.com'
  s.files       = Dir['lib/*.rb', 'lib/vat_validator/*.rb']
  s.license     = 'MIT'
  s.homepage    = 'https://github.com/vpenkoff/vat_validator'

  s.add_dependency 'nokogiri', '~> 1.8'
end
