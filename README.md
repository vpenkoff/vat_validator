# vat_validator
Validates a given VAT number for all european countries using the online European commission service for VAT validation.
See more [here](http://ec.europa.eu/taxation_customs/vies/vatRequest.html).

## How to use it

### As part of a project
Include it in your Gemfile or spec file
```
gem 'vat_validator', :git => 'https://github.com/vpenkoff/vat_validator.git'
```

And then in your project:

```
VatValidator.validate(country_iso_code: "BG", vat_code: "BG213")
```

### Standalone:
Clone the repo and run inside:
```
./bin/vat_validator "BG" "BG12312321"
```

### Example response:
```
{"body":{"countryCode":"BG","vatNumber":"BG123","requestDate":"2017-10-27+02:00","valid":"false","name":"---","address":"---"},"status_code":200}
```
