require 'nokogiri'
module VatValidator
  class VatResponse
    attr_reader :response_body, :response_code

    RESPONSE_DATA_FIELDS = %w[countryCode vatNumber requestDate
                              valid name address faultcode faultstring].freeze

    def initialize(response)
      @response_body = parse_response_body(response)
      @response_code = parse_response_code(response_body)
      self
    end

    def value
      { body: response_body, status_code: response_code }
    end

    private

    def parse_response_body(response)
      doc = Nokogiri::XML(response)
      parse(doc)
    end

    def parse_response_code(parsed_response)
      VatValidator.message_to_code(parsed_response[:faultstring])
    end

    def parse(node, data = {})
      if node.children
        node.children.each do |child|
          if RESPONSE_DATA_FIELDS.include? child.name
            data[child.name.to_sym] = child.children.first.to_s
          else
            parse(child, data)
          end
        end
      end
      data
    end
  end
end
