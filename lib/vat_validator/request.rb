require 'net/http'
module VatValidator
  class VatRequest
    TEST_SERVICE_URL = 'http://ec.europa.eu/taxation_customs/vies/services/checkVatTestService'.freeze
    SERVICE_URL = 'http://ec.europa.eu/taxation_customs/vies/services/checkVatService'.freeze
    HEADERS_CONTENT_TYPE = 'application/x-www-form-urlencoded'.freeze
    HEADERS_ACCEPT = 'text/html,application/xhtml+xml,application/xml,text/xml;q=0.9,*/*;q=0.8'.freeze
    HEADERS_ACCEPT_ENCODING = 'none'.freeze
    HEADERS_ACCEPT_CHARSET = 'utf-8'.freeze
    HEADERS_CONNECTION = 'close'.freeze
    HEADERS_SOAP_ACTION = 'urn:ec.europa.eu:taxud:vies:services:checkVat/checkVat'.freeze

    SOAP_TEMPLATE = <<-HEREDOC.freeze
      <soap:Envelope
        xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"
        soap:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"
        xmlns:tns="urn:ec.europa.eu:taxud:vies:services:checkVat:types">
      <soap:Header>
      </soap:Header>
      <soap:Body>
        <tns:checkVat
            xmlns:tns="urn:ec.europa.eu:taxud:vies:services:checkVat:types"
            xmlns="urn:ec.europa.eu:taxud:vies:services:checkVat:types">
          <tns:countryCode>%s</tns:countryCode>
          <tns:vatNumber>%s</tns:vatNumber>
        </tns:checkVat>
      </soap:Body>
      </soap:Envelope>
    HEREDOC

    attr_reader :request_params, :uri, :request

    def initialize(**request_params)
      @request_params = request_params
      @uri = URI(SERVICE_URL)
      @request = Net::HTTP::Post.new(uri)
      @request.body = format(SOAP_TEMPLATE,
                             request_params[:country_iso_code],
                             request_params[:vat_number])
      setup_headers
    end

    def perform_request
      return dummy_response unless VatValidator::COUNTRIES.include? request_params[:country_iso_code]
      begin
        Net::HTTP.start(uri.host, read_timeout: 5, open_timeout: 1) do |http|
          response = http.request request
          return response.body
        end
      rescue Net::ReadTimeout
        return dummy_response
      end
    end

    private

    def setup_headers
      @request['Content-Type'] = HEADERS_CONTENT_TYPE
      @request['Accept'] = HEADERS_ACCEPT
      @request['Accept-Encoding'] = HEADERS_ACCEPT_ENCODING
      @request['Accept-Charset'] = HEADERS_ACCEPT_CHARSET
      @request['Connection'] = HEADERS_CONNECTION
      @request['Host'] = uri.hostname
      @request['SOAPAction'] = HEADERS_SOAP_ACTION
    end

    def dummy_response
      template = <<-HEREDOC.freeze
      <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
      <soap:Body>
      <checkVatResponse xmlns="urn:ec.europa.eu:taxud:vies:services:checkVat:types">
      <valid>true</valid>
      </checkVatResponse>
      </soap:Body>
      </soap:Envelope>
      HEREDOC
      template
    end
  end
end
