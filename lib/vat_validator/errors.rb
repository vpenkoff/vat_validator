module VatValidator
  module_function

  def message_to_code(message)
    {
      'INVALID_INPUT' => 400,
      'INVALID_REQUESTER_INFO' => 400,
      'SERVICE_UNAVAILABLE' => 503,
      'MS_UNAVAILABLE' => 503,
      'TIMEOUT' => 504,
      'VAT_BLOCKED' => 409,
      'IP_BLOCKED' => 409,
      'GLOBAL_MAX_CONCURRENT_REQ' => 503,
      'GLOBAL_MAX_CONCURRENT_REQ_TIME' => 503,
      'MS_MAX_CONCURRENT_REQ' => 503,
      'MS_MAX_CONCURRENT_REQ_TIME' => 503
    }[message] || 200
  end
end
