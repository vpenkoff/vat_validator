require_relative 'vat_validator/request'
require_relative 'vat_validator/response'
require_relative 'vat_validator/version'
require_relative 'vat_validator/errors'
require_relative 'vat_validator/countries'

module VatValidator
  module_function

  def validate(country_iso_code:, vat_number:)
    response = VatRequest.new(country_iso_code: country_iso_code,
                              vat_number: vat_number).perform_request
    VatResponse.new(response).value
  end
end
